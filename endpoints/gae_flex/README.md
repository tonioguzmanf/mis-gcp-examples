# Google Cloud Endpoints 

Un ejemplo de una API desarrollada en App Engine Flexible (Python runtime) y expuesta mediante Cloud Endpoints. Tiene 
dos métodos, getsong y savesong que permiten consultar y persistir información en datastore. Para usar savesong es 
neceario tener una API Key

## Despliega tu API pública mediante Google Cloud Endpoints

##### 1.

[Crea](https://console.cloud.google.com/projectcreate) un proyecto de Google Cloud Platform o reutiliza alguno 
existente.

##### 2. 

* Asegura tener instalado el [Google Cloud SDK](https://cloud.google.com/sdk/downloads) Puedes no instalarlo y en su 
lugar usa el [Google Cloud Shell](https://console.cloud.google.com/home/dashboard?cloudshell=true). 

##### 3.

* Crea tu entorno virtual

```
virtualenv -p python3 my_ce_venv
source my_ce_venv/bin/activate
```

##### 4.

* Resuelve tus dependencias

```
pip install -r requirements.txt
``` 

##### 5. 

* [Crea](https://console.cloud.google.com/iam-admin/serviceaccounts) una cuenta de servicio con los 
privilegios mínimos necesarios y obtén el correspondiente archivo .json

* Asigna a la variable `GOOGLE_APPLICATION_CREDENTIALS` la ruta de tu archivo .json
  
Ejemplo para Linux y MacOS
```bash
export GOOGLE_APPLICATION_CREDENTIALS="/La/Ruta/De/Tu/Archivo.json"
```
Ejemplo para Windows Power Shell
```
$env:GOOGLE_APPLICATION_CREDENTIALS="C:\La\Ruta\De\Tu\Archivo.json
```

##### 5. 

* Crea tu endpoint basado en la descripción de tu archivo `openapi-appengine.yaml`

```bash
gcloud endpoints services deploy openapi-appengine.yaml
```

* El resultado del comando anterior te da el nombre del servicio que debes sustituir en el atributo `name` del campo 
`endpoints_api_service` dentro de `app.yaml` El nombre del servicio tiene una estructura similar a 
`tu-id-project.appspot.com`

##### 6. 

* Despliega tu endpoint

```bash
gcloud app deploy
```

* Si todo salió bien ahora puedes llamar al método savesong, pero para ello necesitarás un API Key así que gestiónala en
la sección de [Credenciales](https://console.cloud.google.com/apis/credentials) de tu proyecto de GCP.

Notas: 

* Es muy buena práctica acotar el alcance de esa API Key por lo que deberás restrigirla que solo pueda usar la API 
que recién has publicado bajo el nombre definido en el campo `title` en el archivo `openapi-appengine.yaml`

* Puedes probar de manera local ejecutando 

```python
python main.py
```

##### 7. 

* Genera un portal para developers basado en la descripción de tu `openapi-appengine.yaml` accediendo a la sección de 
[Endpoints](https://console.cloud.google.com/endpoints/portal) de tu proyecto. Se creará un developer portal por cada 
servicio que hayas publicado (al menos uno en este caso) y ahora puedes compartilo con tu equipo de desarrolladores para
que se familiaricen con la API y puedan hacer pruebas.

![Developer Portal](img/dev_portal.png)
