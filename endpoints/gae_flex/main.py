import logging

from flask import Flask, jsonify, request
from google.cloud import datastore

app = Flask(__name__)


@app.route('/demo/music/getsong/<genre>', methods=['GET'])
def get_song(genre):
    genre = request.view_args['genre']
    ds_client = datastore.Client()
    query = ds_client.query(kind='song')
    query.add_filter('genre', '=', genre)

    results = list(query.fetch(limit=3))

    song = {'name': results[0]['name'],
            'url': results[0]['url']}

    logging.info('Cloudendpoints se integra con Stackdriver y te permite gestionar tus logs.')

    return jsonify(song)


@app.route('/demo/music/savesong', methods=['POST'])
def save_song():
    ds_client = datastore.Client()
    song = request.get_json()

    with ds_client.transaction():
        entity_key = ds_client.key('song')
        task = datastore.Entity(key=entity_key)

        task.update({
            'genre': song['genre'],
            'mood': song['mood'],
            'name': song['name'],
            'singer': song['singer'],
            'url': song['url']
        })

        ds_client.put(task)

        return jsonify({'status_message': 'Disfrutaremos tu esa rolita que nos has compartido :-)'})


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)
