# Google Kubernetes Engine

## Horizontal Pod Autoscaling y Node Autoprovisioning.

### Despliega tu cluster de Kubernetes usando Google Kubernetes Engine

##### 1.

[Crea](https://console.cloud.google.com/projectcreate) un proyecto de Google Cloud Platform o reutiliza alguno 
existente.

##### 2. 

Asegura tener instalado el [Google Cloud SDK](https://cloud.google.com/sdk/downloads) Puedes no instalarlo y en su 
lugar usa el [Google Cloud Shell](https://console.cloud.google.com/home/dashboard?cloudshell=true). 

##### 3.

* Declara las siguientes varibles
```
export CLUSTER_NAME=el_nombre_quieras_darle
export CLUSTER_ZONE=la_zona_que_mejor_convenga
export PROJECT_ID=el_project_id_de_tu_proyecto
```


* Crea un cluster standard con Kubernetes Engine

```
gcloud container --project $PROJECT_ID clusters create $CLUSTER_NAME --zone $CLUSTER_ZONE
```

* Conéctate al cluster

```
gcloud container clusters get-credentials $CLUSTER_NAME --zone $CLUSTER_ZONE --project $PROJECT_ID
```

### Echa a andar tu aplicación

##### 4. 

* Despliega tu aplicación (definida en [nginx.yaml](nginx.yaml))

```
kubectl apply -f nginx.yaml
```

* Verifica que tu aplicación y servicio se desplegaron correctamente.

```
kubectl get deployments
kubectl get services
```

Si todo salió bien, deberías tener un deployment y un servicio, de tipo LoadBalancer, llamados nginx y nginx-service.

### Define el modo y criterio(s) de escalamiento

##### 5.

* Despliega el HorizontalPodAutoscaler (definido en [nginx-hpa.yaml](nginx-hpa.yaml))

```
kubectl apply -f nginx-hpa.yaml
```

* Verifica que tu HorizontalPodAutoscaler se desplegó correctamente.

```
kubectl get hpa
```

### Haz que escale tu app

Para este caso se optó por escalamiento basado en CPU. El uso promedio no debería ser mayor al 50%

##### 6.

* Instala una utilería para generar carga hacia tu aplicación.

```
sudo apt install siege
```

* Genera carga simulando 255 usuarios concurrentes.

```
siege -c 255 http://EXTERNAL_IP_DEL_SERVICE
```

Puedes variar la carga de acuerdo a las necesidades y ritmo de cambio que desees ver en tu aplicación y su escalamiento. Para notar las variaciones del uso de CPU, resultará útil ejecutar: 

```
kubectl get hpa
```

Podrás notar que el número de réplicas crece hasta los límites definidos por tu deployment y la capacidad de los nodos de tu cluster. Verifícalo con:

```
kubectl get pods
```

No olvides que puedes detener la utilería `siege` con la combinación de teclas `ctrl+c`.

### Explora otras opciones de escalamiento en GKE

##### 7.

Habiendo probado las capacidades del HorizontalPodAutoscaler

* Incrementa las capacidades de escalamiento de los nodos de tu cluster, en tu node pool.

```
gcloud container clusters update $CLUSTER_NAME --enable-autoscaling --min-nodes 1 --max-nodes 10 --zone $CLUSTER_ZONE
```

* Para priorizar la optimización de recursos cambia el perfil del autoscaling

```
gcloud container clusters update $CLUSTER_NAME --autoscaling-profile optimize-utilization --zone $CLUSTER_ZONE
```

* Para ver reflejadas estas nuevas capacidades, repite la generación de carga como se indica en el punto 6.

Con el autoscaling notaste que crece el número de nodos en tu default node pool. 

Notarás que el número de nodos disminuye, luego de algunos minutos, tras haber detenido la utilería `siege`.

##### 8. 

Habiendo probado las capacidades del AutoScaling

* Incrementa las capacidades de escalamiento de los nodos de tu cluster, creando nuevos nodos en node pools diferentes al default.

```
gcloud container clusters update $CLUSTER_NAME --enable-autoprovisioning --zone $CLUSTER_ZONE --max-cpu 18 --max-memory 36
```

* Para ver reflejadas estas nuevas capacidades, repite la generación de carga como se indica en el punto 6.

Con el autoprovisioning notaste que crece el número de nodos, en tu default node pool, y que se añaden nuevos nodos en otro pool con máquinas que pueden atender de mejor forma las necesidades de escalamiento de tu aplicación. 

Notarás que el número de nodos disminuye, luego de algunos minutos, tras haber detenido la utilería `siege`.

### Documentación

* [Horizontal Pod Autoscaling](https://cloud.google.com/kubernetes-engine/docs/how-to/horizontal-pod-autoscaling)

* [Cluster Autoscaler](https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-autoscaler)

* [Node auto-provisioning](https://cloud.google.com/kubernetes-engine/docs/how-to/node-auto-provisioning)

##### Notas

* No pierdas de vista que puedes modificar valores definidios en este ejemplo, pero sin olvidar que puedes incurrir en gastos significativos.

* Comentarios, sugerencias y correcciones son bienvenidos.
