# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Ejemplo de Big Query para ilustrar una consulta a un public dataset y luego habilitar el cache para no incurrir en costo
repitiendo la consulta mediante la misma cuenta de servicio y acelerar el tiempo de respuesta evitando que Big Query
nuevamente calcule el resultado.
"""

from google.cloud import bigquery

bigquery_client = bigquery.Client()

# Los 25 nombres de mayor frecuencia en EUA
QUERY = 'SELECT name, SUM(number) as count ' \
        'FROM `bigquery-public-data.usa_names.usa_1910_current` ' \
        'GROUP BY name ' \
        'ORDER BY count DESC ' \
        'LIMIT 25'

bigquery_query_job = bigquery_client.query(QUERY)

# Un job puede tomar varios segundos en finalizar así que el resultado se devuelve hasta que ha terminado.
rows = bigquery_query_job.result()

for row in rows:
    print(row)


# Big Query te permite configurar diferentes elementos de la consulta a ejecutar.
query_job_config = bigquery.QueryJobConfig()
# Habilitamos cache para evitar que Big Query repita el proceso de consulta y recupere la respuesta del cache que se
# produjo con la consulta previa. Si el cache no existe o ya no tiene disponibilidad, se ejecuta la consulta otra vez
# Siempre qe el cache exista NO incurrimos en costos y los tiempos, que ya son buenos, se mejoran.
query_job_config.use_query_cache = True

# Para simplificar el procesamiento de la respuesta conviene exportar los resultado a un pandas dataframe
df = bigquery_client.query(query=QUERY, job_config=query_job_config).to_dataframe()

# Cambio el index para que comience a contar desde el 1 y no desde el 0.
df.index = range(1, 26)
# Asigno nuevos nombres a las columnas
df.columns = ['Nombre', 'Ocurrencias']

print(df)


# Esta consulta mueve 4.06[TB] Recuerda que incurre en costos si la ejecutas, especialmente si no habilitas cache
"""
QUERY = 'SELECT language, SUM(views) AS views ' \
        'FROM `bigquery-samples:wikipedia_benchmark.Wiki100B` ' \
        'WHERE REGEXP_MATCH(title,"G,*o.*o.*g") ' \
        'GROUP BY language ' \
        'ORDER BY views DESC'
"""
