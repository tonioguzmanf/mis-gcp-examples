import os
import psycopg2

params = {
    "host": os.environ['DB_HOST'],
    "database": os.environ['DB_NAME'],
    "user": os.environ['DB_USER'],
    "password": os.environ['DB_PASS']
}

def hello_gcs(event, context):
    """Triggered by a change to a Cloud Storage bucket.
    Args:
         event (dict): Event payload.
         context (google.cloud.functions.Context): Metadata for the event.
    """
    file = event
    print(f"Processing file: {file['name']}.")
    print(params)
    print(get_db_version())

def get_db_version():
    conn = None

    conn = psycopg2.connect(**params)
    cur = conn.cursor()

    cur.execute('SELECT version()')
    db_version = cur.fetchone()

    cur.close()

    conn.close()

    return str(db_version)
