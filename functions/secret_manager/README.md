# Google Cloud Functions 

Un ejemplo de una Google Cloud Function desarrollada en Python. Tiene por objeto integrarse con Secret Manager 
para mantener cifrada, y fuera del alcance de quienes no tienen genuino interés, la información que por su naturaleza
tiene un caracter sensible.


##### 1.

[Crea](https://console.cloud.google.com/projectcreate) un proyecto de Google Cloud Platform o reutiliza alguno 
existente.

##### 2. 

* Asegura tener instalado el [Google Cloud SDK](https://cloud.google.com/sdk/downloads) Puedes no instalarlo y en su 
lugar usa el [Google Cloud Shell](https://console.cloud.google.com/home/dashboard?cloudshell=true). 

* Asegura que tienes inicializado el proyecto correcto

```bash
gcloud config set project [PROJECT_ID]
```

##### 3.

* Crea tu entorno virtual

```
python3 -m venv /path/to/new/virtual/environment
```

* Resuelve tus dependencias

```
pip install -r requirements.txt
``` 

##### 4.  

* [Crea](https://console.cloud.google.com/iam-admin/serviceaccounts) una cuenta de servicio con los 
privilegios mínimos necesarios y obtén el correspondiente archivo .json

* Asigna a la variable `GOOGLE_APPLICATION_CREDENTIALS` la ruta de tu archivo .json
  
Ejemplo para Linux y MacOS
```bash
export GOOGLE_APPLICATION_CREDENTIALS="/La/Ruta/De/Tu/Archivo.json"
```
Ejemplo para Windows Power Shell
```
$env:GOOGLE_APPLICATION_CREDENTIALS="C:\La\Ruta\De\Tu\Archivo.json
```

##### 5. 
* Crea tu secreto

```bash
gcloud secrets create [SECRET_ID] --data-file="/path/to/file.txt"
```

##### 6. 
* Despliega tu función

```bash
gcloud beta functions deploy [FUNCTION_NAME] \
--entry-point [ENTRY_POINT] \
--runtime [RUNTIME] \
--set-secrets 'ENV_VAR_NAME=SECRET:VERSION,...,ENV_VAR_NAME-n=SECRET-n:VERSION-n' \
--trigger-resource [YOUR_TRIGGER_BUCKET_NAME] \
--trigger-event [YOUR_TRIGGER_EVENT] \
--ingress-settings [INGRESS_SETTINGS] \
--vpc-connector [CONNECTOR_NAME] \
--egress-settings [EGRESS_SETTINGS] \
--service-account [EMAIL_SA_ADDRESS]
```

* Para que la cuenta de servicio tenga acceso a los secrets en Secret Manager, asigna el rol adecuado.

```bash
gcloud projects add-iam-policy-binding [PROJECT_ID] \
--member='serviceAccount:[EMAIL_SA_ADDRESS]' \
--role='roles/secretmanager.secretAccessor'
```


* Si todo salió bien ahora puedes probar tu función subiendo un archivo. Revisa los logs de tu función
para confirmar el resultado esperado.

### Documentación

* [Integración con Secret Manager](https://cloud.google.com/functions/docs/configuring/secrets)
* [Creación de secretos](https://cloud.google.com/secret-manager/docs/creating-and-accessing-secrets)
* [Triggers de las funciones](https://cloud.google.com/functions/docs/concepts/events-triggers)
* [Opciones de red](https://cloud.google.com/functions/docs/networking/network-settings)
* [Identidad de las funciones](https://cloud.google.com/functions/docs/securing/function-identity)
* [Despliegue de la función](https://cloud.google.com/functions/docs/deploying)


#### Mejoras para el código y notificaciones por detección de errores siempre son bienvenidas :-)
