# Google Cloud Storage

Un ejemplo de las posibilidades de Google Cloud a través una app

## Configura y echa a andar tu app

##### 1.

[Crea](https://console.cloud.google.com/projectcreate) un proyecto de Google Cloud Platform o reutiliza alguno 
existente.

##### 2. 

* Asegura tener instalado el [Google Cloud SDK](https://cloud.google.com/sdk/downloads) Puedes no instalarlo y en su 
lugar usa el [Google Cloud Shell](https://console.cloud.google.com/home/dashboard?cloudshell=true). 

##### 3.

###### 3.1

Clona el repositorio

```bash
git clone https://gitlab.com/tonioguzmanf/mis-gcp-examples
```

Ubícate ene l directorio adecuado

```bash
cd mis-gcp-examples/storage/demo-quiz-app
```

###### 3.2

Da permisos de ejecución al archivo que configura tu ambiente

```bash
chmod u+x prepare_environment.sh
```

Ejecuta el script

```bash
./prepare_environment.sh
```

Crea un bucket que tenga el nombre de tu Project_Id y concaténale "-media"

```bash
gsutil mb gs://$DEVSHELL_PROJECT_ID-media
```

Exporta la variable de ambiente

```bash
export GCLOUD_BUCKET=$DEVSHELL_PROJECT_ID-media
```

###### 3.3

Inicia la ejecución de tu aplicación

```bash
python run_server.py
```

###### 3.4

Accede a tu aplicación y sube alguna imagen. Verifica que haya quedado almadenada en tu bucket


#### Mejoras para el código y notificaciones por detección de errores siempre son bienvenidas :-)

