# Google Cloud Storage

Un ejemplo de las posibilidades de Google Cloud a través del Google Cloud SDK usando el comando gsutil

## Haz una gestión versátil de tus archivos

##### 1.

[Crea](https://console.cloud.google.com/projectcreate) un proyecto de Google Cloud Platform o reutiliza alguno 
existente.

##### 2. 

* Asegura tener instalado el [Google Cloud SDK](https://cloud.google.com/sdk/downloads) Puedes no instalarlo y en su 
lugar usa el [Google Cloud Shell](https://console.cloud.google.com/home/dashboard?cloudshell=true). 

##### 3.

###### 3.1

Descarga una imagen

```bash
curl https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Ada_Lovelace_portrait.jpg/800px-Ada_Lovelace_portrait.jpg --output ada.jpg
```

###### 3.2

Crea un bucket

```bash
gsutil mb gs://TU_BUCKET
```

###### 3.3

Almacena un archivo en Cloud Storage

```bash
gsutil cp /LA/RUTA/DE/TU/ARCHIVO/ada.jpg gs://TU_BUCKET
```

###### 3.4

Elimina tu archivo local

```bash
rm /LA/RUTA/DE/TU/ARCHIVO/ada.jpg
```

Ahora descárgalo nuevamente desde Cloud Storage

```bash
gsutil cp -r gs://TU_BUCKET/ada.jpg .
```

###### 3.5

Habilitar versionamiento

```bash
gsutil versioning set on gs://TU_BUCKET
```

Conoce el estatus del versionamiento

````bash
gsutil versioning get gs://TU_BUCKET
````

Copia nuevamente tu archivo para generar una segunda versión

```bash
gsutil cp /LA/RUTA/DE/TU/ARCHIVO/ada.jpg gs://TU_BUCKET
```

Lista toda las versiones existentes y sus respectivos identificadores únicos

````bash
gsutil ls -a gs://TU_BUCKET/TU_FILE
````

Recupera una versión específica y produce un nuevo archivo

````bash
gsutil cp gs://TU_BUCKET/TU_FILE#GENERATION_NUMBER_X gs://BUCKET_DESTINO/NUEVO_NOMBRE_DE_TU_FILE
````

Borra una versión específica

````bash
gsutil rm gs://TU_BUCKET/TU_FILE#GENERATION_NUMBER_X
````

###### 3.6

Controla el nivel de acceso a tu objeto.

Hazlo de acceso público

```bash
gsutil acl ch -u AllUsers:R gs://TU_BUCKET/ada.jpg
```

Remueve el acceso público

```bash
gsutil acl ch -d AllUsers gs://TU_BUCKET/ada.jpg
```

###### 3.7

Define una política de retención de objetos

```bash
gsutil retention set 123s gs://TU_BUCKET
```

Consulta la política de retención

```bash
gsutil retention get gs://TU_BUCKET
```

Produce un nuevo archivo

```bash
gsutil cp gs://TU_BUCKET/ada.jpg gs://TU_BUCKET/NUEVO_NOMBRE_DE_TU_FILE
```

Consulta los detalles de tu nuevo archivo 

```bash
gsutil ls -L gs://TU_BUCKET/NUEVO_NOMBRE_DE_TU_FILE
```

Intenta borrar tu archivo

```bash
gsutil rm gs://TU_BUCKET/NUEVO_NOMBRE_DE_TU_FILE
```

###### 3.8

Revisa en la consola web de Cloud Storage las opciones para manejar Locks y Holds de políticas 
de retención. 

Si deseas borrar tu bucket, siempre que no haya obejtos retenidos por alguna política, ejecuta

```bash
gsutil rb gs://TU_BUCKET/
```

#### Mejoras para el código y notificaciones por detección de errores siempre son bienvenidas :-)
