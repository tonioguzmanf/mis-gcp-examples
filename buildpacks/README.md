# Google Cloud Buildpacks

### Deploy a Cloud Run Services without using a Dockerfile. Thanks Google Cloud Builpacks ;-)

##### 1.

Clone this repo 
```
git clone https://gitlab.com/tonioguzmanf/mis-gcp-examples
cd mis-gcp-examples/buildpacks
```

##### 2. 

Just execute the next command
```
gcloud run deploy [YOUR_SERVICE_NAME] --source .
```

If everything went well, now you have a Cloud Run service deployed on one Google Cloud region serving a simple Python web app. 

Executing the previous command is equivalent to running:

```
gcloud builds submit --pack image=[IMAGE] .
gcloud run deploy [SERVICE] --image [IMAGE]
```

Behind the scenes Cloud Buildpacks and Cloud Build created the Container image that was deployed.

### Documentation

* [Google Cloud Buildpacks](https://github.com/GoogleCloudPlatform/buildpacks)

* [Cloud Run deploying from Source Code](https://cloud.google.com/run/docs/deploying-source-code)

* [Cloud Native Buildpacks](https://buildpacks.io/)
