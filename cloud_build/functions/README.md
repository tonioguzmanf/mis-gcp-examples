# Google Cloud Build
Un ejemplo de cómo desplegar una Google Cloud Function mediante un trigger de Google Cloud Build. 
Se define como trigger a las modificaciones en el repositorio, Google Cloud Source para este caso, que sean acompañadas 
por una tag.

##### 1.

[Crea](https://console.cloud.google.com/projectcreate) un proyecto de Google Cloud Platform o reutiliza alguno 
existente.

##### 2. 

Asegura tener instalado el [Google Cloud SDK](https://cloud.google.com/sdk/downloads) Puedes no instalarlo y en su 
lugar usa el [Google Cloud Shell](https://console.cloud.google.com/home/dashboard?cloudshell=true).

##### 3. 

Conecta tu repositorio local Git con [Google Cloud Source Repositories](https://console.cloud.google.com/code/develop/repo) 
(también los puedes hacer con [GitHub y Bitbucket](https://cloud.google.com/source-repositories/docs/connecting-hosted-repositories)).

Nota: Como mecanismo de prueba rápida sugerimos usar el ejemplo de 
[Google DLP](https://gitlab.com/tonioguzmanf/mis-gcp-examples/blob/master/dlp/dlp_custom_detection/dlp_custom_detection.py)

##### 4. 

[Añade](https://console.cloud.google.com/cloud-build/triggers/add) el trigger de Cloud Build definiendo las tags para 
desencadenar el build.

##### 5.

Personaliza tu ```cloudbuild.yaml``` con los datos de tu Cloud Function y tu Cloud Storage bucket:

```yaml
steps:
- name: 'gcr.io/cloud-builders/gcloud'
  args:
  - functions
  - deploy
  - EL_NOMBRE_DE_TU_FUNCION
  - --source=.
  - --runtime=python37
  - --trigger-event=google.storage.object.finalize
  - --trigger-resource=EL_NOMBRE_DE_TU_BUCKET
```

##### 6.

* Crea tu tag
```bash
git tag -a v1.23 -m "Mi tag que será usada para desencadenar la construcción y despliegue de una Cloud Function"
```

* Agrega todos tus archivos involucrados
```bash
git add .
```

* Haz el commit correspondiente
```bash
git commit -m "el commit que lo inicia todo :-p"
```

* Haz el push incluyendo la tag creada
```bash
git push -u origin master v1.23
```

##### 7.

Si todo salió bien ahora puedes consultar el [historial de builds](https://console.cloud.google.com/cloud-build/builds) 
y ver el estado de tu build. Cuando haya finalizado correctamente puedes consultar tu listado de 
[Cloud Function](https://console.cloud.google.com/functions/list) para encontrarla ahí y comenzar a usarla.

#### Mejoras para el código y notificaciones por detección de errores siempre son bienvenidas :-)
