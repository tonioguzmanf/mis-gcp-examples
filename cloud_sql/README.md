# Google Cloud Storage

Un ejemplo de una Base de Datos relacional en Google Cloud.

## Haz una gestión versátil de tus archivos

##### 1.

[Crea](https://console.cloud.google.com/projectcreate) un proyecto de Google Cloud Platform o reutiliza alguno 
existente.

##### 2. 

* Asegura tener instalado el [Google Cloud SDK](https://cloud.google.com/sdk/downloads) Puedes no instalarlo y en su 
lugar usa el [Google Cloud Shell](https://console.cloud.google.com/home/dashboard?cloudshell=true). 

##### 3.

###### 3.1
* Crea una instancia de base de datos

```bash
gcloud sql instances create [TU-INSTANCIA] --database-version=POSTGRES_14 --cpu=[número_de_vCPU] --memory=[GB_de_RAM]
```

Nota: El valor de la RAM debe estar entre 0.90 GiB y 6.50 GiB por vCPU


* Asigna un password al usuario predeterminado

```bash
gcloud sql users set-password postgres --instance=[TU-INSTANCIA] --password=[EL-PASSWORD-PARA-TU-USER]
```

###### 3.2
* Conéctate a la instancia de Cloud SQL que creaste

```bash
gcloud sql connect [TU-INSTANCIA] --user=postgres
```

###### 3.3
* Crea una tabla y cárgale algunos datos de muestra

```sql
CREATE TABLE rolita (nombre VARCHAR(123), cantante VARCHAR(123), entryID SERIAL PRIMARY KEY);
INSERT INTO rolita (nombre, cantante) values ('Tenement Yard', 'Jacob Miller');
INSERT INTO rolita (nombre, cantante) values ('I am that I am', 'Peter Tosh');
```

* Consulta tus datos para confirmar que tus datos se insertaron

```bash
SELECT * FROM rolita;
```

###### 3.4
* Crea una DB

```bash
gcloud sql databases create [TU-DATABASE] --instance=[TU-INSTANCIA]
```

* Listas las DB existentes

```bash
gcloud sql databases list --instance=[TU-INSTANCIA]
```

###### 3.5
* Crea un nuevo usuario

```bash
gcloud sql users create [TU-USER] --instance=[TU-INSTANCIA] --password=[EL-PASSWORD-PARA-TU-USER]
```

###### 3.6 
* Importa datos a la DB que creaste

```bash
gcloud sql import csv [TU-INSTANCIA] gs://TU-BUCKET/TU-ARCHIVO --database=[TU-DATABASE] --table=[TU-TABLA]
```

Nota: La tabla NO existe en la DB que creaste así que: 

* Usa el usuario que creaste previamente para conectarte a la instancia

```bash
gcloud sql connect [TU-INSTANCIA] --user=[TU-USER] --database=[TU-DATABASE]
```

* Crea la tabla y finalmente reintenta el import de datos

```sql
CREATE TABLE rolita (nombre VARCHAR(123), cantante VARCHAR(123), entryID SERIAL PRIMARY KEY);
```

###### 3.7
* Haz un backup de la DB que poblaste

```bash
gcloud sql backups create --async --instance [INSTANCE_NAME]
```

###### 3.8
* Usa [esta guía](https://console.cloud.google.com/sql/instances?walkthrough_id=sql__quickstart-sql-gce__quickstart-sql-gce-index) 
para seguir el paso a paso que te permite establecer una conexión al motor de DB, desde una VM


###### 3.9
* Establece una conexión a tu DB usando el Cloud SQL Proxy. Comienza descargándalo

```bash
wget https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 -O cloud_sql_proxy
```

* Dale permisos de ejecución

```bash
chmod u+x cloud_sql_proxy
```

* Ejecuta el proxy

```bash
./cloud_sql_proxy -instances=[TU-PROJECT-ID:REGION-INSTANCIA:TU-INSTANCIA]=tcp:5432 &
```

* Usa el cliente psql

```bash
psql -U [TU-USER] --host=127.0.0.1
```

#### Mejoras para el código y notificaciones por detección de errores siempre son bienvenidas :-)

